package cn.com.chiner.java.command.impl;

import cn.com.chiner.java.command.ExecResult;
import cn.com.chiner.java.dialect.DBDialect;
import cn.com.chiner.java.dialect.DBDialectMatcher;
import cn.com.chiner.java.fisok.raw.kit.JdbcKit;
import cn.com.chiner.java.fisok.sqloy.core.DBType;
import cn.com.chiner.java.fisok.sqloy.kit.DBTypeKit;
import cn.com.chiner.java.model.TableEntity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.URLDecoder;
import java.sql.*;
import java.util.Map;

/**
 * ParseDDLToTableV2Impl
 *
 * @author moujf <br>
 * @version 1.0 <br>
 * @date 2022/8/9 14:26 <br>
 */
public class ParseDDLToTableV2Impl  extends AbstractDBCommand<ExecResult> {
    protected Logger logger = LoggerFactory.getLogger(getClass());

    @Override
    public ExecResult exec(Map<String, String> params) {
        super.init(params);
        ExecResult ret = new ExecResult();
        Connection conn = null;
        TableEntity tableEntitie = null;
        try {
            String ddlContent = params.get("ddl");
            ddlContent = URLDecoder.decode(ddlContent, "UTF-8");
            conn = createConnect();
            tableEntitie = createTable(conn,ddlContent);
            ret.setStatus(ExecResult.SUCCESS);
            ret.setBody(tableEntitie);
        } catch (Exception e) {
            ret.setStatus(ExecResult.FAILED);
            ret.setBody(e.getMessage());
            logger.error("", e);
        } finally {
            JdbcKit.close(conn);
        }
        return ret;
    }

    private TableEntity createTable(Connection conn,String ddlScript) throws SQLException, IOException {
        Statement stmt = conn.createStatement();
        String[] tableSqls = ddlScript.split(";");
        String createSql = tableSqls[0].toUpperCase();
        String tableName = createSql.substring(createSql.indexOf("TABLE") + 6, createSql.indexOf("("));
        tableName = tableName.replaceAll("\\s*|\r|\n|\t", "");
        ResultSet rs =conn.getMetaData().getTables(null, null, tableName, null);
        if(rs.next()) {
            stmt.execute("drop table "+tableName);
        }
        for(String sql : tableSqls) {
            stmt.execute(sql);
        }
        DatabaseMetaData meta = conn.getMetaData();
        DBType dbType = DBTypeKit.getDBType(meta);
        DBDialect dbDialect = DBDialectMatcher.getDBDialect(dbType);
        TableEntity tableEntity = dbDialect.createTableEntity(conn, meta, tableName);
        return tableEntity;
    }
}
